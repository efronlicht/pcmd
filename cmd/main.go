package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"time"

	"gitlab.com/efronlicht/pcmd/pkg/color"
	"gitlab.com/efronlicht/pcmd/pkg/pcmd"
)

var Timeout time.Duration

func init() {
	flag.DurationVar(&Timeout, "timeout", 5*time.Minute, "timeout commands after")
	flag.Var(color.Mode, "color", "terminal ANSI color")
}

func fatalf(format string, args ...interface{}) {
	fmt.Fprintln(os.Stderr, color.RED.Apply("fatal error: ")+fmt.Sprintf(format, args...))
	os.Exit(1)
}
func main() {
	flag.Parse()
	ctx, cancel := context.WithTimeout(context.Background(), Timeout)
	defer cancel()
	err := pcmd.Run(ctx, os.Stdin, os.Stdout, os.Stderr)
	if err != nil {
		fatalf("running commands: %v", err)
	}
}
