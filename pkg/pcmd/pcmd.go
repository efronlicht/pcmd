package pcmd

import (
	"bufio"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"text/tabwriter"

	"gitlab.com/efronlicht/pcmd/pkg/color"
)

type Command struct {
	// Dir is the working directory of the command. This will be passed to the exec.Cmd created.
	Dir string
	// Args are command-line arguments. At least one (the command name or path) is required.
	Args []string
	// Env are additional environment variables. Zero value is OK.
	Env map[string]string
}

type Result struct {
	label          string
	cmd            Command
	stdout, stderr *os.File
	err            error
}

func Run(ctx context.Context, stdin io.Reader, stdout, stderr io.Writer) error {
	var commands map[string]Command
	if err := json.NewDecoder(stdin).Decode(&commands); err != nil {
		return fmt.Errorf("reading stdin: could not unmarshal input to %T: %v", commands, err)
	}

	results := make(chan Result, len(commands))
	stdout = tabwriter.NewWriter(stdout, 4, 4, 2, ' ', 0)
	stderr = tabwriter.NewWriter(stderr, 4, 4, 2, ' ', 0)
	defer stdout.(*tabwriter.Writer).Flush()
	defer stderr.(*tabwriter.Writer).Flush()
	fmt.Fprintln(stderr, "LABEL\tDIR\tMESSAGE")
	fmt.Fprintln(stderr, "------\t------\t------")
	for k, c := range commands {
		if len(c.Args) == 0 {
			return fmt.Errorf(`%q: expected at least one entry for "args"`, k)
		}
		fmt.Fprintf(stderr, "%s\t%s\t%s\n", k, prettyDir(c.Dir), color.BLUE.Apply("STARTING"))
		k, c := k, c
		go func() {
			results <- run(ctx, k, c)
		}()
	}
	var failures int
	for range commands {
		r := <-results
		if r.err == nil {
			log.New(stderr, r.logPrefix(), 0).Println(color.GREEN.Apply("SUCCESS"))
			continue
		} else {
			failures++
			r.logFailure(stdout, stderr)
		}
		go r.stdout.Close()
		go r.stderr.Close()
	}
	if failures > 0 {
		return fmt.Errorf("failed %3d of %3d jobs", failures, len(commands))
	}
	return nil
}

func prettyDir(dir string) string {
	if dir == "" {
		return "."
	}
	return dir
}

func (r *Result) logPrefix() string {
	return fmt.Sprintf("%s\t%s\t", r.label, prettyDir(r.cmd.Dir))
}

func (r *Result) logFailure(stdout, stderr io.Writer) {
	prefix := r.logPrefix()
	w, e := log.New(stdout, prefix, 0), log.New(stderr, prefix, 0)
	redprint := func(logger *log.Logger, s string) {
		logger.Println(color.RED.Apply(s))
	}
	redprint(e, "error: "+r.err.Error())
	scanner, ok := bufio.NewScanner(r.stdout), false
	for scanner.Scan() {
		if !ok {
			ok = true
			redprint(e, "DUMPING STDOUT")
		}
		w.Printf("\t%s\n", scanner.Bytes())
	}
	if err := scanner.Err(); err != nil {
		e.Fatalf("SCANNING STDOUT: \t%s\n", err)
	}
	scanner, ok = bufio.NewScanner(r.stderr), false
	for scanner.Scan() {
		if !ok {
			ok = true
			redprint(e, "DUMPING STDERR")
		}
		e.Printf("\t%s\n", scanner.Bytes())
	}
	if err := scanner.Err(); err != nil {
		e.Fatalf("SCANNING STDERR: \t%s\n", err)
	}
}

func run(ctx context.Context, label string, c Command) (r Result) {

	cmd := exec.CommandContext(ctx, c.Args[0], c.Args[1:]...)
	if len(c.Env) != 0 {
		cmd.Env = os.Environ()
		for k, v := range c.Env {
			cmd.Env = append(cmd.Env, fmt.Sprintf("%s=%s", k, v))
		}
	}
	r.label = label
	var err error
	r.stdout, err = ioutil.TempFile("", fmt.Sprintf("%s_%s_*", label, "stdout"))
	if err != nil {
		panic(fmt.Errorf("could not open temp file: %v", err))
	}
	r.stderr, err = ioutil.TempFile("", fmt.Sprintf("%s_%s_*", label, "stderr"))
	if err != nil {
		panic(fmt.Errorf("could not open temp file: %v", err))
	}
	r.cmd = c
	bufOut, bufErr := bufio.NewWriter(r.stdout), bufio.NewWriter(r.stderr)
	cmd.Stdout, cmd.Stderr = bufOut, bufErr
	r.err = cmd.Run()
	bufOut.Flush()
	bufErr.Flush()
	return r
}
