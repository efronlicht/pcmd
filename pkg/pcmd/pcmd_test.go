package pcmd_test

import (
	"bytes"
	"context"
	"encoding/json"
	"os"
	"strings"
	"testing"
	"time"

	"gitlab.com/efronlicht/pcmd/pkg/pcmd"
)

func TestMain(m *testing.M) {
	os.Exit(m.Run())
}
func runTest(raw string) error {
	reader := bytes.NewReader(json.RawMessage(raw))
	ctx, cancel := context.WithTimeout(context.Background(), 100*time.Millisecond)
	defer cancel()
	return pcmd.Run(ctx, reader, os.Stdout, os.Stderr)
}
func Test_Run_Success(t *testing.T) {
	t.Parallel()
	err := runTest(`{
		"echo": {"args": ["echo", "true"]},
		"true": {"args": ["true"]}
	}`)
	if err != nil {
		t.Fatal("expected no error, but got", err)
	}
}

func Test_Run_BadInput(t *testing.T) {
	t.Parallel()
	err := runTest(`foo`)
	if !strings.Contains(err.Error(), "unmarshal") {
		t.Fatal(err)
	}
	err = runTest(`{"no args": {"args": []}}`)
	if !strings.Contains(err.Error(), "expected at least one") {
		t.Fatal(err)
	}

}

func Test_Run_Timeout(t *testing.T) {
	t.Parallel()
	if testing.Short() {
		t.Skip("SKIP Test_Run_Timeout: sleeps")
	}
	err := runTest(`{"too long": {"args": ["sleep", "1"]}}`)
	if err == nil {
		t.Fatal("expected a timeout error, but got nil")
	}
}

func Test_Run_Failure(t *testing.T) {
	t.Parallel()
	cmd := map[string]pcmd.Command{
		"echo":    {Args: []string{"echo", "foo"}},
		"foo":     {Args: []string{"false"}},
		"bar":     {Args: []string{"false"}},
		"go test": {Args: []string{"test", "foo"}},
	}
	b, _ := json.Marshal(cmd)
	err := runTest(string(b))
	if err == nil {
		t.Fatal("expected an error, but got nil")
	}
}
