package color

import (
	"flag"
	"fmt"
	"os"
	"strings"
	"sync/atomic"

	"golang.org/x/term"
)

var Mode = &mode{n: ModeAuto}

type mode struct {
	n int64
}

var _ flag.Value = Mode

func (m *mode) Set(s string) error {
	switch strings.ToLower(s) {
	case "off":
		m.SetOff()
	case "on":
		m.SetOn()
	case "auto":
		m.SetAuto()
	default:
		return fmt.Errorf(`unknown mode %q: available modes are "off", "on", and "auto" (default)`, s)
	}
	return nil
}
func (m *mode) String() string {
	switch m.n {
	case ModeOff:
		return "off"
	case ModeOn:
		return "on"
	case ModeAuto:
		return "auto"
	default:
		panic("unknown mode!")
	}
}

const (
	ModeOff  = 0b00
	ModeOn   = 0b01
	ModeAuto = 0b10
)

func (m *mode) IsOn() bool {
	n := atomic.LoadInt64(&m.n)
	return n == ModeOn || n == ModeAuto && StderrIsTerminal()
}
func (m *mode) IsOff() bool {
	n := atomic.LoadInt64(&m.n)
	return n == 0 || n == ModeAuto && !StderrIsTerminal()
}
func (m *mode) SetOn()       { atomic.StoreInt64(&m.n, ModeOn) }
func (m *mode) SetOff()      { atomic.StoreInt64(&m.n, ModeOff) }
func (m *mode) SetAuto()     { atomic.StoreInt64(&m.n, ModeAuto) }
func StderrIsTerminal() bool { return term.IsTerminal(int(os.Stderr.Fd())) }
func StdoutIsTerminal() bool { return term.IsTerminal(int(os.Stdout.Fd())) }

type Color struct{ escape string }

var (
	RED   = Color{"\033[0;31m"}
	NONE  = Color{"\033[0m"}
	GREEN = Color{"\033[0;32m"}
	BLUE  = Color{"\033[0;34m"}
)

func (c Color) Apply(s string) string {
	if Mode.IsOn() {
		return fmt.Sprintf("%s%s%s", c.escape, s, NONE.escape)
	}
	return s
}
func (c Color) Name() string {
	switch c {
	case RED:
		return "Color.RED"
	case NONE:
		return "Color.NONE"
	case GREEN:
		return "Color.GREEN"
	case BLUE:
		return "Color.BLUE"
	default:
		return fmt.Sprintf("Color.<UNKNOWN: %s>", c.escape)
	}
}
func (c Color) String() string {
	if Mode.IsOn() {
		return fmt.Sprintf("%s%s%s", c.escape, c.Name(), NONE.escape)
	}
	return c.Name()
}
