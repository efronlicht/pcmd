# PCMD 
**P**arallel Co**m**man**d** runner.

PCMD runs commands in parallel.  It runs silently until all of it's commands are completed or it's timeout (default 1 minute) completes.

It dumps the stdout and stderr of those failed processes to stdout and stderr respectively.

PCMD is terminal-aware and will color it's output only when outputting to a terminal.

## install
### from source (linux, macOS, bsd)
- go 1.16 or later.
```sh
git clone https://gitlab.com/efronlicht/pcmd
cd pcmd/cmd
go build -o pcmd main.go  # build the binary as "pcmd"
mv pcmd /usr/local/bin # move the built binary to /usr/local/bin (anywhere else in your path is fine!)
```
### downloading a pre-compiled binary 
Go to the [pcmd-bin]( https://gitlab.com/efronlicht/pcmd-bin) repository.
#### the current release is **v00.00**
#### currently supported os-architecture pairs are as follows
- macOS/amd64
- macOS/arm64 ("apple silicon"
- windows/amd64
- linux/amd64
- linux/arm64
## usage
pipe properly formatted JSON to stdin. 

`cat` is probably the easiest way.
```
pcmd [timeout=1m] [color=auto] << INPUT
```
### JSON format:
```json
    {
        "most-basic": {"args": ["some command"]},
        "more-complicated-example": {
            "args": ["the-command", "one", "or more", "additional args"],
            "environment": {
                "some_environment_variable": "some value",
                "these_are_in": "addition to the outside environment",
                "is_this_param_optional": "absolutely"
            },
            "dir": "directory to run it in, relative to the current working directory",
        }
    }
```
### example
```
+ cat example.json | pcmd
LABEL   DIR     MESSAGE
------  ------  ------
printf  .       STARTING
echo    .       STARTING
echo    .       SUCCESS
printf  .       SUCCESS
```

